﻿using System;
using System.Collections.Generic;

namespace RomansNumber.services
{
    public class NumberConvertService
    {
        private static Dictionary<char, int> romanMap = new Dictionary<char, int>()
        {
            {'I', 1},
            {'V', 5},
            {'X', 10},
            {'L', 50},
            {'C', 100},
            {'D', 500},
            {'M', 1000}
        };
        public NumberConvertService()
        {

        }

        #region Converter
        /*
        *   @reference https://stackoverflow.com/questions/14900228/roman-numerals-to-integers
        *   @modify Alexandre Bucar Dubina
        *   @param string romanNumber 
        *   @description Function convert Romans number to  Int/ Funcao convert numero romano para inteiro
        *   Algoritimo simples utilizando Dictonary    
        */
        public int RomansToInt(string romanNumebr)
        {
            int number = 0;
            try
            {
                for (int i = 0; i < romanNumebr.Length; i++)
                {
                    if (i + 1 < romanNumebr.Length && romanMap[romanNumebr[i]] < romanMap[romanNumebr[i + 1]])
                    {
                        number -= romanMap[romanNumebr[i]];
                    }
                    else
                    {
                        number += romanMap[romanNumebr[i]];
                    }
                }
            }catch(Exception e)
            {
                Console.WriteLine("Opção de numero inteiro inválida favor rever o input de dados");
            }
            return number;
        }


        /*
         *   @reference https://stackoverflow.com/questions/7040289/converting-integers-to-roman-numerals
         *   @modify Alexandre Bucar Dubina
         *   @param int number number 
         *   @description Function convert Int to Romans number / Funcao convert inteiro para romano 
         *   Algoritimo super simples, com o limite máximo de conversao o peso da recursividade fica irrelevante  
         */

        public string IntToRomans(int number)
        {
            if (number > 3000) return number.ToString();

            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + IntToRomans(number - 1000);
            if (number >= 900) return "CM" + IntToRomans(number - 900);
            if (number >= 500) return "D" + IntToRomans(number - 500);
            if (number >= 400) return "CD" + IntToRomans(number - 400);
            if (number >= 100) return "C" + IntToRomans(number - 100);
            if (number >= 90) return "XC" + IntToRomans(number - 90);
            if (number >= 50) return "L" + IntToRomans(number - 50);
            if (number >= 40) return "XL" + IntToRomans(number - 40);
            if (number >= 10) return "X" + IntToRomans(number - 10);
            if (number >= 9) return "IX" + IntToRomans(number - 9);
            if (number >= 5) return "V" + IntToRomans(number - 5);
            if (number >= 4) return "IV" + IntToRomans(number - 4);
            if (number >= 1) return "I" + IntToRomans(number - 1);

            return number.ToString();

        }
        #endregion
    }
}
