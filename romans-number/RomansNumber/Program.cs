﻿using RomansNumber.services;
using System;

namespace RomansNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberConvertService romansNumber = new NumberConvertService();
            string result = string.Empty;
            Console.Write("Conversor de numeros: \n");
            Console.Write("Para converter numeros inteiros em romanos aperte (r). \n");
            Console.Write("Para converter numeros romanos em inteiros aperte (i). \n");
            ConsoleKeyInfo cki = Console.ReadKey();
            if (cki.Key.ToString().ToLower() == "r")
            {
                Console.Write("\nInforme um numero inteiro entre 1 - 3000. \n");
                string i = Console.ReadLine();
                int inteiro;
                Int32.TryParse(i, out inteiro);
                result = romansNumber.IntToRomans(inteiro);
            }
            else if (cki.Key.ToString().ToLower() == "i")
            {
                Console.Write("\nInforme um numero romano caracteres aceitos (I, V, X, L, C, D, M). \n");
                string roman = Console.ReadLine();
                result = romansNumber.RomansToInt(roman.ToUpper()).ToString();
            }
            else
            {
                Console.Write("\nOpção não valida\n");
                Console.Write("\nAperte qualquer tecla para fechar.... \n");
                System.Environment.Exit(1);
            }
            Console.Write("\nResultado da conversão: " + result);
            Console.Write("\nAperte qualquer tecla para fechar.... \n");
            Console.ReadKey();
            System.Environment.Exit(1);
        }
    }
}
