using RomansNumber.services;
using System;
using Xunit;

namespace RomansNumbers.Tests
{
    public class NumberConvertTest
    {
        private readonly NumberConvertService numberConvertService;
        public NumberConvertTest()
        {
            numberConvertService = new NumberConvertService();
        }

        [Theory]
        [InlineData(1, "I")]
        [InlineData(7, "VII")]
        [InlineData(10, "X")]
        [InlineData(15, "XV")]
        [InlineData(31, "XXXI")]
        [InlineData(100, "C")]
        [InlineData(150, "CL")]
        [InlineData(500, "D")]
        [InlineData(1000, "M")]
        [InlineData(1003, "MIII")]
        [InlineData(1004, "MIV")]
        [InlineData(1155, "MCLV")]
        [InlineData(1500, "MD")]
        [InlineData(2800, "MMDCCC")]
        [InlineData(2890, "MMDCCCXC")]
        [InlineData(3000, "MMM")]
        [InlineData(3001, "3001")]
        public void IntToRomans(int value, string equals)
        {
            string result = numberConvertService.IntToRomans(value);
            Assert.Equal(result, equals);
        }


        [Theory]
        [InlineData("I", 1)]
        [InlineData("VII", 7)]
        [InlineData("X", 10)]
        [InlineData("XV", 15)]
        [InlineData("XXXI", 31)]
        [InlineData("C", 100)]
        [InlineData("CL", 150)]
        [InlineData("D", 500)]
        [InlineData("M", 1000)]
        [InlineData("MIII", 1003)]
        [InlineData("MIV", 1004)]
        [InlineData("MCLV", 1155)]
        [InlineData("MD", 1500)]
        [InlineData("MMDCCC", 2800)]
        [InlineData("MMDCCCXC", 2890)]
        [InlineData("MMM", 3000)]
        [InlineData("MMMI", 3001)]
        public void RomansToInt(string value, int equals)
        {
            int result = numberConvertService.RomansToInt(value);
            Assert.Equal(result, equals);
        }
    }
}
